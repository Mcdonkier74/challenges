#!/bin/bash

for symbolx in ` grep 'Record Publish' answer | awk '{print $3,$4}' | sed 's/ /:/' | sort -u`
do
	#Change : to space
	symbol=$(echo "$symbolx" | sed 's/:/ /')

	# get the data for each symbol
	grep  -A2 "$symbol" answer | 

awk "BEGIN {
	trade_size=0;
	trade_volume=0;
}

\$1 ~ /wTradePrice/ {
trade_size+=\$7;
}

\$1 ~ /wTradeVolume/ {
trade_volume+=\$7;
}

END {
    print \"$symbol\";
    print \"Total Trade Price = £\"trade_size;
    print \"Total Volume Traded = \"trade_volume;
}"
done

cat answer | egrep 'wTradePrice' |  awk '{ SUM += $7;} END { print"Total Trade Price = £" SUM }' && cat answer | egrep 'wTradeVolume' |  awk '{ SUM += $7;} END { print"Total Trade Volume = " SUM }'


