#!/bin/bash

#remove comments
cat hosts.real  | sed -e 's/#.*$//' -e '/^$/d'|  awk '$1=$1' > newhosts
# Get the Mac addresses
cat hosts.real | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'
#Grab the ips and put in a file called ips
grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' hosts.real > ips
#remove the extra spaces and mv hosts.real
#newhosts | awk '$1=$1'

mv newhosts hosts.real
